@extends('layouts.app')

@section('content')
<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 48px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }
</style>

    <div class="content">
        <div class="title m-b-md">
            Портал помощи людям, </br>попавшим в сложную жизненную ситуацию
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-6">
            <label><h4>Если Вам нужна помощь:</h4></label>
            <label>Этот сайт создан специально для Вас! </br>Для получения помощи Вам  сначала нужно </br>пройти регистрацию по <a href="{{route('register')}}">ссылке</a> и перейти в <a href="{{route('profile')}}">профиль</a> </br>
            Затем разместить заявку на помощь</label>
        </div>
        <div class="col-12 col-sm-6">
            <label><h4>Если Вы хотите помочь:</h4></label>
            <label>Дорогие волонтеры, добровольцы, помощники! </br>Огромное количество людей в данную минуту 
            нуждается в Вашей помощи, переходите по ссылке в <a href="{{route('order.index')}}">список</a> заявок, </br>
            не оставайтесь равнодушными</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6">
            <label></label>
        </div>
        <div class="col-12 col-sm-6">
            <label></label>
        </div>
    </div>
</div>
@endsection