@extends('layouts.app')
<style>
    .filters {
        background-color: #F9F9F9;
    }
    .order {
        background-color: #FFF;
    }
    .extinfo {
        background-color: #F9F9F9;
    }
</style>
@section('content') 
 
<h2 class="txter" align="center">Заявка.</h2>

<div class="container border border-success rounded order">
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Краткое описание: </label>
        </div>
        <div class="col-12 col-sm-9">
            <label>{{ $order->shortdescription ?? "" }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Подробное описание:</label>
        </div>
        <div class="col-12 col-sm-9">
            <label>{{ $order->description ?? "" }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Статус заявки:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->status ?? "" }}</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>Приблизительный размер помощи в рублях:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->cost ?? "" }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Город:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->city ?? "" }}</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>Район:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->district ?? "" }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Улица:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->street ?? "" }}</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>Номер дома:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->house ?? "" }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Номер квартиры:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->flat ?? "" }}</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>Ближайшее метро:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->metro ?? "" }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-3">
            <label>Номер телефона:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->telefone ?? "" }}</label>
        </div>
        <!-- <div class="col-12 col-sm-3">
            <label>Рейтинг:</label>
        </div>
        <div class="col-12 col-sm-3">
            <label>{{ $order->rating ?? "" }}</label>
        </div> -->
    </div>
</div>

</br></br></br>

<h4 class="txter" align="center">Дополнительная информация по заявке</h4>

@foreach ($comments ?? [] as $comment)

    <div class="container border border-success rounded extinfo">
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Имя:</label>
            </div>
            <div class="col-12 col-sm-9">
                <label>{{ $comment->user_name }}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Сообщение:</label>
            </div>
            <div class="col-12 col-sm-9">
                <label>{{ $comment->comment }}</label>
            </div>
        </div>
    </div>
    </br>
@endforeach
<div class="container">
    <div class="row">
        {{ $comments->links() }}
    </div>
</div>

@if (Auth::user())
    <form method="POST" action="{{ route('comment.store') }}">
        @csrf
        <input type="hidden" name="order_id" value="{{ $order->id ?? "" }}"/>
        <input type="hidden" name="user_id" value="{{ Auth::user()->id ?? "" }}"/>
        <input type="hidden" name="user_name" value="{{ Auth::user()->name ?? "" }}"/>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <label>Имя: {{ Auth::user()->name ?? "" }}</label>
                </div>
                <div class="col-12">
                    <textarea class="form-control" rows="2" name="comment" required maxlength="254"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-9">
                    <label></label>
                </div>
                <div class="col-12 col-sm-3">
                    <label></label>
                    <input id="submit" class="input form-control value="Отправить" type="submit" />
                </div>
            </div>
        </div>
    </form>
@endif

@endsection