@extends('layouts.app')
<style>
    .filters {
        background-color: #F9F9F9;
    }
    .orders {
        background-color: #FFF;
    }
</style>
@section('content')

<!-- <form method="GET" action="{{ route('order.filter') }}">
@csrf
    <div class="container border filters rounded">
        <div class="row">
            <div class="col-12" align="center">
                <label>Фильтры:</label>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Город:</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>
                    <select class="form-control" name="city" id="city">
                        <option></option>
                        <option>Санкт-Петербург</option>
                        <option>Москва</option>
                        <option>Екатеринбург</option>
                        <option>Челябинск</option>
                    </select>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label><input class="btn btn-primary btn-sm" type="submit" value="       Искать      "></label>
            </div>
            <div class="col-12 col-sm-3">
                <label><input class="btn btn-primary btn-sm" onclick="document.getElementById('clearFilter').submit();" type="button" value="Сбросить фильтр"></label>
            </div>
            <div class="col-12 col-sm-3">
                <label></label>
            </div>
        </div>
    </div>
</form>
<form method="GET" action="{{ route('order.index') }}" id="clearFilter"></form> -->

<div align="center">
    <h3 class="txter">Список заявок.</h3>
</div>

<div class="container orders">
    <div class="row rounded">

        <div class="col-12 col-md-3">
            <div class="container orders">
                <label></label>
                <div class="row rounded">
                <form method="GET" action="{{ route('order.filter') }}">
                    @csrf
                        <div class="container border filters rounded">
                            <div class="row">
                                <div class="col-12" align="center">
                                    <label>Фильтры:</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label>Город:</label>
                                </div>
                                <div class="col-12">
                                    <label>
                                        <select class="form-control" name="city" id="city">
                                            <option></option>
                                            <option>Санкт-Петербург</option>
                                            <option>Москва</option>
                                            <option>Екатеринбург</option>
                                            <option>Челябинск</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label><input class="btn btn-primary btn-sm" type="submit" value="       Искать      "></label>
                                </div>
                                <div class="col-12">
                                    <label><input class="btn btn-primary btn-sm" onclick="document.getElementById('clearFilter').submit();" type="button" value="Сбросить фильтр"></label>
                                </div>
                                <div class="col-12">
                                    <label></label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form method="GET" action="{{ route('order.index') }}" id="clearFilter"></form>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-9">
            @foreach ($orders as $order)
                <div class="container orders">
                    <label></label>
                    <div class="row border border-success rounded filters">
                        <div class="col-12 col-sm-3">
                            <label>Краткое описание:</label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label><a href="{{ route('order.show.id', $order->id) }}" target="_blank">{{ $order->shortdescription }}</a></label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>Цена:</label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>{{$order->cost}}</label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>Город:</label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>{{ $order->city }}</label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>Дата:</label>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>{{ $order->updated_at }}</label>
                        </div>
                    </div>
                </div>
                <br/>
            @endforeach
            <div class="container">
                <div class="row">
                    {{ $orders->links() }}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


