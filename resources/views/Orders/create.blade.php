@extends('layouts.app')

<head>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.min.js" type="text/javascript"></script>
</head>

@section('content')

<h2 class="txter" align="center">Форма заявки</h2>

@if ((Auth::check() == 1) && (Auth::id() == $users->id))

<form method="POST" action="{{ route('order.store') }}" class="txter">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Краткое описание</label>
            </div>
            <div class="col-12 col-sm-9">
                <input class="input form-control" name="shortdescription" type="text" value="" required/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Подробное описание</label>
            </div>
            <div class="col-12 col-sm-9">
                <textarea class="input form-control" name="description"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Статус заявки</label>
            </div>
            <div class="col-12 col-sm-3">
                <select class="form-control" name="status" id="status">
                    <option>Активная</option>
                    <option>Не активная</option>
                </select>
            </div>
            <div class="col-12 col-sm-3">
                <label>Размер помощи в рублевом эквиваленте</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="cost" type="text" maxlength="4" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Город</label>
            </div>
            <div class="col-12 col-sm-3">
                <select class="form-control" name="city" id="city" required>
                    <option></option>
                    <option>Санкт-Петербург</option>
                    <option>Москва</option>
                    <option>Екатеринбург</option>
                    <option>Челябинск</option>
                </select>
            </div>
            <div class="col-12 col-sm-3">
                <label>Район</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="district" type="text form-control" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Улица</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="street" type="text form-control" value=""/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Номер дома</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="house" type="text" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Номер квартиры</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="flat" type="text" value=""/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Ближайшее метро</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="metro" type="text" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Номер телефона</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="telefone" type="tel" id="phone_1" value="" required/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Рейтинг</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="rating" type="text" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Электронная почта</label>
            </div>
            <div class="col-12 col-sm-3">
                <input type="email" aria-describedby="emailHelp" class="input form-control" name="email" value=""/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Пользователь</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="user" type="text" value=""/>
            </div>
        </div>
        <input class="input" name="email" type="hidden" value="{{ $users->email }}"/>
        <input class="input" name="user" type="hidden" value="{{ $users->name }}"/>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label></label>
                <input id="submit" class="input form-control value="Отправить" type="submit" />
            </div>
        </div>
    </div>
</form>
<script>
    $("#phone_1").mask("+7(999) 999-9999");
</script>
@else
<div class="txter">
    У Вас не достаточно прав для просмотра этой страницы.
</div>
@endif

@endsection