@extends('layouts.app')

<head>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.min.js" type="text/javascript"></script>
</head>

@section('content')
<h2 class="txter" align="center">Редактирование формы заявки.</h2>

@if ((Auth::check() == 1) && (Auth::user()->name == ($order->user ?? "") ))

<form method="POST" action="{{ route('order.update.id', $order->id) }}" class="txter">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Краткое описание</label>
            </div>
            <div class="col-12 col-sm-9">
                <input class="input form-control" name="shortdescription" type="text" value="{{ $order->shortdescription }}" required/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Подробное описание</label>
            </div>
            <div class="col-12 col-sm-9">
                <textarea class="input form-control" name="description">{{ $order->description }}</textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Статус заявки</label>
            </div>
            <div class="col-12 col-sm-3">
                <select class="form-control" name="status" id="status">
                    <option>{{ $order->status }}</option>
                    @if ($order->status == 'Активная')
                    <option>Не активная</option>
                    @else
                    <option>Активная</option>
                    @endif
                </select>
            </div>
            <div class="col-12 col-sm-3">
                <label>Размер помощи в рублевом эквиваленте</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="cost" type="text" maxlength="4" value="{{ $order->cost }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Город</label>
            </div>
            <div class="col-12 col-sm-3">
                <select class="form-control" name="city" id="city" required>
                    <option @if ($order->city == 'Санкт-Петербург') selected @endif>Санкт-Петербург</option>
                    <option @if ($order->city == 'Москва') selected @endif>Москва</option>
                    <option @if ($order->city == 'Екатеринбург') selected @endif>Екатеринбург</option>
                    <option @if ($order->city == 'Челябинск') selected @endif>Челябинск</option>
                </select>            </div>
            <div class="col-12 col-sm-3">
                <label>Район</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="district" type="text form-control" value="{{ $order->district }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Улица</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="street" type="text form-control" value="{{ $order->street }}"/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Номер дома</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="house" type="text" value="{{ $order->house }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Номер квартиры</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="flat" type="text" value="{{ $order->flat }}"/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Ближайшее метро</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="metro" type="text" value="{{ $order->metro }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Номер телефона</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="telefone" type="text" id="phone_1" value="{{ $order->telefone }}" required/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Рейтинг</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="rating" type="text" value="{{ $order->rating }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Электронная почта</label>
            </div>
            <div class="col-12 col-sm-3">
                <input type="email" aria-describedby="emailHelp" class="input form-control" name="email" value="{{ $users->email }}"/>
            </div>
            <div class="col-12 col-sm-3">
                <label>Пользователь</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="user" type="text" value="{{ $users->name }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4">
                <label></label>
                <input id="submit" class="input form-control value="Отправить" type="submit" />
            </div>
        </div>
    </div>
</form>
<script>
    $("#phone_1").mask("+7(999) 999-9999");
</script>
@else
<div class="txter">
    У Вас не достаточно прав для просмотра этой страницы.
</div>
@endif

@endsection