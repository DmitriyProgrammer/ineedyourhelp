@extends('layouts.app')

@section('content')
 
<h2 class="txter" align="center">Форма редактирования профиля.</h2>

@if ((Auth::check() == 1) && (Auth::id() == $users->id))

<form method="POST" action="{{ route('profile.update') }}" class="txter">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Электронная почта*</label>
            </div>
            <div class="col-12 col-sm-3">
                <input class="input form-control" name="email" type="text" value="{{ $users->email }}"/>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-12 col-sm-3">
                <input id="submit" class="input form-control" value="Отправить" type="submit" />
            </div>
        </div>
    </div>
</form>

@else
<div class="txter">
    У Вас не достаточно прав для просмотра этой страницы.
</div>
@endif
@endsection