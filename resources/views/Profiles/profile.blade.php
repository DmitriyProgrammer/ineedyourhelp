@extends('layouts.app')

@section('content')

@if ((Auth::check() == 1) && (Auth::id() == $users->id))

    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <label><a href="{{route('profile.edit', $users->id)}}" target="_blank" class="btn btn-primary">Редактировать профиль</a></label>
            </div>
            @if ( count($orders) <3)
            <div class="col-12 col-sm-3">
                <label><a href="{{route('order.create')}}" class="btn btn-primary" target="_blank">Добавить заявку на помощь</a></label>
            </div>
            @else
            @endif
        </div>
        <br/>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Имя пользователя:</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>{{ $users->name }}</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>Email пользователя:</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>{{ $users->email }}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3">
                <label>Дата создания:</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>{{ $users->created_at }}</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>Дата изменения:</label>
            </div>
            <div class="col-12 col-sm-3">
                <label>{{ $users->updated_at }}</label>
            </div>
        </div>
        <hr/>
        <h3 align="center">Ваши заявки</h3>
        @foreach ( ($orders ?? []) as $order)
        <div class="row">
            <div class="col-12 col-md-1">
                <label>Заявка:</label>
            </div>
            <div class="col-12 col-md-2">
                <label>{{$order->status}}</label>
            </div>
            <div class="col-12 col-md-3">
                <label>{{$order->shortdescription}}</label>
            </div>
            <div class="col-12 col-md-2">
                <label><a href="{{route('order.show.id', $order->id)}}" target="_blank" class="btn btn-primary">Просмотр</a></label>
            </div>
            <div class="col-12 col-md-2">
                <label><a href="{{route('order.edit.id', $order->id)}}" target="_blank" class="btn btn-primary">Редактировть</a></label>
            </div>
            <div class="col-12 col-md-2">
                <label><a href="{{route('order.destroy.id', $order->id)}}" target="_blank" class="btn btn-primary">Удалить</a></label>
            </div>
        </div>
        @endforeach

    </div>

    @else
    <div class="txter">
        У Вас не достаточно прав для просмотра этой страницы.
    </div>
@endif
@endsection