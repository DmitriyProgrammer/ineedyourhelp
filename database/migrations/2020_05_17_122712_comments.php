<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Comments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('comments', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('deleted')->default(0);
        $table->string('_token')->nullable();
        $table->string('comment')->nullable();
        $table->string('user_name')->nullable();
        $table->string('user_id')->nullable();
        $table->string('order_id')->nullable();
        $table->string('checked')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
