<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deleted')->default(0);
            $table->string('_token')->nullable();
            $table->string('shortdescription');
            $table->string('cost')->nullable();
            $table->string('description')->nullable();
            $table->string('foto')->nullable();
            $table->string('status')->nullable();
            $table->string('formhelp')->nullable();
            $table->string('city');
            $table->string('district')->nullable();
            $table->string('street')->nullable();
            $table->string('house')->nullable();
            $table->string('flat')->nullable();
            $table->string('metro')->nullable();
            $table->string('rating')->nullable();
            $table->string('telefone');
            $table->string('email')->nullable();
            $table->string('user')->nullable();
            $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
