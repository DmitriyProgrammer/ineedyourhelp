<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Models\Orders\Order;
use Illuminate\Http\Request;
use Auth;
use DB;

class OrdersController extends Controller
{

    public function index()
    {
        $orders = DB::table('orders')->where('deleted', 0)->where('status', 'Активная')->paginate(50);
        return view('orders.index', ['orders' => $orders]);
    }

    public function create()
    {
        $user = Auth::user();
        return view('orders.create', ['users' => $user]);
    }

    public function store(Request $request)
    {
        $order = new Order;
        $order->shortdescription = $request->shortdescription;
        if($request->cost > 9000){$request->cost = 9000;}
        $order->cost = $request->cost;
        $order->description = $request->description;
        $order->foto = $request->foto;
        $order->status = $request->status;
        $order->formhelp = $request->formhelp;
        $order->city = $request->city;
        $order->district = $request->district;
        $order->street = $request->street;
        $order->house = $request->house;
        $order->flat = $request->flat;
        $order->metro = $request->metro;
        $order->rating = $request->rating;
        $order->telefone = $request->telefone;
        $order->email = $request->email;
        $order->user = $request->user;
        $order->save();

        return redirect()->route('profile');
    }

    public function show(Order $order, $id)
    {
        $comments = [];
        $comments = DB::table('comments')->where('deleted', 0)->where('order_id', $id)->paginate(10);
        $order = DB::table('orders')->where('deleted', 0)->where('id', $id)->first();
        return view('orders.show', ['order' => $order, 'comments' => $comments]);
    }

    public function edit($id)
    {
        $order = DB::table('orders')->where('deleted', 0)->where('id', $id)->first();
        $user = Auth::user();
        return view('orders.edit', ['users' => $user, 'order' => $order]);
    }

    public function update(Request $request, Order $order, $id)
    {
        if( Order::whereId( $id )->update( $request->all() ) )
        {
            return redirect()->route('profile'); 
        }
        return response()->json('Something went wrong', 500);
    }

    public function destroy(Order $orders, $id)
    {
        $order = DB::table('orders')->where('id', $id)->first();
        if($order->user ==  Auth::user()->name) {
            DB::update('update orders set deleted = ? where id = ?', [1, $id]);
        }
        return redirect()->route('profile');
    }

    public function filter(Request $request) {

        if($request['city'] == '') {
            $orders = DB::table('orders')->where('deleted', 0)->where('status', 'Активная')->paginate(50);
            return view('orders.index', ['orders' => $orders]);
        }
        $orders = DB::table('orders')->where('deleted', 0)->where('status', 'Активная')->where('city', $request['city'])->paginate(50);
        return view('orders.index', ['orders' => $orders]);
    }

}