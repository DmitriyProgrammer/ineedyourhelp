<?php

namespace App\Http\Controllers\Profiles;

use App\Http\Controllers\Controller;
use App\Models\Profiles\Profile;
use App\Models\Orders\Order;
use Illuminate\Http\Request;
use DB;
use Auth;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $users = DB::table('users')->where('id', $id)->first();
        return view('profiles.profile', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if($user = Auth::user()){
            $orders = DB::table('orders')->where('user', Auth::user()->name)->where('deleted', 0)->get();
            return view('profiles.profile', ['users' => $user, 'orders' => $orders]);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profiles, $id)
    {
        if(Auth::user()){
            if(Auth::user()->id == $id) {
                $users = DB::table('users')->where('id', $id)->first();
                return view('profiles.edit', ['users' => $users]);
            } else {
                $user = Auth::user();
                return view('profiles.profile', ['users' => $user]);
            }
        } else {
            return view('profiles.profile');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profiles)
    {
        // $profiles->name = request('name');
        $profiles->email = request('email');
        DB::update('update users set email = ? where id = ?', [request('email'), Auth::user()->id]);
        return redirect()->route('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profiles $profiles)
    {
        //
    }
}
