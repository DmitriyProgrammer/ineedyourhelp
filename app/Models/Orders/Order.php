<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['id','deleted','_token','shortdescription', 'cost', 
        'description', 'foto', 'status', 'formhelp',
        'city', 'district', 'street', 'house',
        'flat', 'metro', 'rating', 'telefone',
        'email', 'user'];
}
