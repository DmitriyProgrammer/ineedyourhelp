<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'Profiles\ProfilesController@show')->name('profile');
Route::get('/profile/{id}', 'Profiles\ProfilesController@index')->name('profile.id');
Route::get('/profile/edit/{id}', 'Profiles\ProfilesController@edit')->name('profile.edit');   //TO DO!!!!!!!!!!!!! add id
Route::post('/profile/update', 'Profiles\ProfilesController@update')->name('profile.update');

Route::get('/order/create', 'Orders\OrdersController@create')->name('order.create');
Route::post('/order/store', 'Orders\OrdersController@store')->name('order.store');
Route::get('/order/edit/{id}', 'Orders\OrdersController@edit')->name('order.edit.id');
Route::post('/order/update/{id}', 'Orders\OrdersController@update')->name('order.update.id');
Route::get('/order/index', 'Orders\OrdersController@index')->name('order.index');
Route::get('/order/show/{id}', 'Orders\OrdersController@show')->name('order.show.id');
Route::get('/order/destroy/{id}', 'Orders\OrdersController@destroy')->name('order.destroy.id');
Route::get('/order/filter', 'Orders\OrdersController@filter')->name('order.filter');

Route::post('/comment/store', 'Comments\CommentsController@store')->name('comment.store');